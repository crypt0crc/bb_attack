# BB - Attack

![Alt text](/images/BB-attack.png)


_BB attack is a tool developed as proof of concept, for the non-permitted control of ICOCO and AWOX bulbs.
The application implements the "Disco" attack along with a DOS attack, through which no client can connect to the device._


## Pre requirements 📋

_For running the requirements are :_

```
Bluepy library : wget https://github.com/IanHarvey/bluepy.git
```
```
Gatttool(check installation)
```
```
python
```

## Instalation 🔧

_To install the requirements:_

_Bluepy install debian :_

```
$ sudo apt-get install python-pip libglib2.0-dev
$ sudo pip install bluepy
```

_Bluepy install Fedora :_

```
$ sudo dnf install python-pip glib2-devel
```

_For Python 3, you may need to use pip3:_

```
$ sudo apt-get install python3-pip libglib2.0-dev
$ sudo pip3 install bluepy
```
_For Python 3, you may need to use pip3:_
```
$ sudo apt-get install git build-essential libglib2.0-dev
$ git clone https://github.com/IanHarvey/bluepy.git
$ cd bluepy
$ python setup.py build
$ sudo python setup.py install
```
_An online version of this is currently available at: http://ianharvey.github.io/bluepy-doc/_

_Gatttool install Kali_
```
apt install gatttool
```
_Gatttool install Debian_

```
$ wget https://www.kernel.org/pub/linux/bluetooth/bluez-5.18.tar.xz

$ dpkg --get-selections | grep -v deinstall | grep bluez

$ tar xvf bluez-5.18.tar.xz

$ sudo apt-get install libglib2.0-dev libdbus-1-dev libusb-dev libudev-dev libical-dev systemd libreadline-dev

$ .configure --enable-library

$ make -j8 && sudo make install

$ sudo cp attrib/gatttool /usr/local/bin/
```

## Run the Script ⚙️

_$ python bb_attack.py_

### Principal Menu  🔩

_Scan Vulnerable devices_

```
Check if vulnerable devices are available.
```

_Obtain Services and Data_

```
Get services and data of all available devices.
```

_Attack ICOCO_

```
Launch attack menu for ICOCO devices. First we need to scan!.
```

_Attack AWOX_

```
Launch attack menu for AWOX devices. First we need to scan!.
```

_Exit_

```
Close Script and exit.
```
#### Attack Menu AWOX 🛠️

_1. Select color_

```
Choose one of the colors to change.
```

_2. Select White lights_

```
Change mode of the bulb light.
```

_3. Turn Off Bulb_

```
Turno off the Bulb.
```

_4. Disco Attack_

```
Launch disco attack, changing the lights all time until you cancel it.
```

_5. DOS attack_

```
Prevents anyone from connecting to the device.
```

_6. Back_

```
Go back to the previous menu!
```

_7. Exit_

```
Close Script and exit.
```
#### Attack Menu ICOCO 🛠️

_1. Select color_

```
Choose one of the colors to change.
```

_2. Turn Off Bulb_

```
Turno off the Bulb.
```

_3. Disco Attack_

```
Launch disco attack, changing the lights all time until you cancel it.
```

_4. DOS attack_

```
Prevents anyone from connecting to the device.
```

_5. Back_

```
Go back to the previous menu!
```

_6. Exit_

```
Close Script and exit.
```

## Authors ✒️

_Developed by :_

**David Cámara** - *Initial source code Code Review and Documentation* - [@crypt0crc,@pieldepanceta]

**Álvaro Temes** - *Initial source code and Code Review* - [@Nimcu]


## License 📄

This project uses code from the bluez project, which is available under the Version 2 of the GNU Public License.
Also thanks to IanHarvey for the bluepy module.

The Python files are released into the public domain by their authors, David Cámara and Álvaro Temes.

## Thanks us 🎁

* Share the project 📢
* Invite a beer to us 🍺. 
* Give us public thanks 🤓.


